-- HOBlockCipher.hs: abstraction for the different BlockCipher classes, plus crazy CFB mode stuff
-- Copyright © 2016  Clint Adams
-- This software is released under the terms of the Expat license.
-- (See the LICENSE file).
{-# LANGUAGE PackageImports #-}

module Codec.Encryption.OpenPGP.Internal.HOBlockCipher
  ( HOBlockCipher(..)
  ) where

import qualified "cryptonite" Crypto.Cipher.Types as CCT

import qualified Data.ByteString as B

class HOBlockCipher cipher where
  cipherInit :: B.ByteString -> Either String cipher
  cipherName :: cipher -> String
  cipherKeySize :: cipher -> CCT.KeySizeSpecifier
  blockSize :: cipher -> Int
  cfbEncrypt ::
       cipher -> B.ByteString -> B.ByteString -> Either String B.ByteString
  cfbDecrypt ::
       cipher -> B.ByteString -> B.ByteString -> Either String B.ByteString
  paddedCfbEncrypt ::
       cipher -> B.ByteString -> B.ByteString -> Either String B.ByteString
  paddedCfbEncrypt = cfbEncrypt
  paddedCfbDecrypt ::
       cipher -> B.ByteString -> B.ByteString -> Either String B.ByteString
  paddedCfbDecrypt = cfbDecrypt

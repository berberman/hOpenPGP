-- PrettyUtils.hs: prettyprinter helpers
-- Copyright © 2018  Clint Adams
-- This software is released under the terms of the Expat license.
-- (See the LICENSE file).
module Codec.Encryption.OpenPGP.Types.Internal.PrettyUtils where

import qualified Data.ByteString.Lazy as BL
import Data.Text.Encoding (decodeUtf8With)
import Data.Text.Encoding.Error (lenientDecode)
import Data.Text.Prettyprint.Doc (Doc, Pretty(..))

prettyBS = pretty . decodeUtf8With lenientDecode

prettyLBS :: BL.ByteString -> Doc ann
prettyLBS = pretty . decodeUtf8With lenientDecode . BL.toStrict

-- Ontology.hs: OpenPGP (RFC4880) "is" functions
-- Copyright © 2012-2019  Clint Adams
-- This software is released under the terms of the Expat license.
-- (See the LICENSE file).
module Codec.Encryption.OpenPGP.Ontology
  (
 -- * for signature payloads
    isCertRevocationSig
  , isRevokerP
  , isPKBindingSig
  , isSKBindingSig
  , isSubkeyBindingSig
  , isSubkeyRevocation
  , isTrustPkt
 -- * for signature subpackets
  , isCT
  , isIssuerSSP
  , isIssuerFPSSP
  , isKET
  , isKUF
  , isPHA
  , isRevocationKeySSP
  , isSigCreationTime
  ) where

import Codec.Encryption.OpenPGP.Types

isCertRevocationSig :: SignaturePayload -> Bool
isCertRevocationSig (SigV4 CertRevocationSig _ _ _ _ _ _) = True
isCertRevocationSig _ = False

isRevokerP :: SignaturePayload -> Bool
isRevokerP (SigV4 SignatureDirectlyOnAKey _ _ h u _ _) =
  any isRevocationKeySSP h && any isIssuerSSP u
isRevokerP _ = False

isPKBindingSig :: SignaturePayload -> Bool
isPKBindingSig (SigV4 PrimaryKeyBindingSig _ _ _ _ _ _) = True
isPKBindingSig _ = False

isSKBindingSig :: SignaturePayload -> Bool
isSKBindingSig (SigV4 SubkeyBindingSig _ _ _ _ _ _) = True
isSKBindingSig _ = False

isSubkeyRevocation :: SignaturePayload -> Bool
isSubkeyRevocation (SigV4 SubkeyRevocationSig _ _ _ _ _ _) = True
isSubkeyRevocation _ = False

isSubkeyBindingSig :: SignaturePayload -> Bool
isSubkeyBindingSig (SigV4 SubkeyBindingSig _ _ _ _ _ _) = True
isSubkeyBindingSig _ = False

isTrustPkt :: Pkt -> Bool
isTrustPkt (TrustPkt _) = True
isTrustPkt _ = False

isCT :: SigSubPacket -> Bool
isCT (SigSubPacket _ (SigCreationTime _)) = True
isCT _ = False

isIssuerSSP :: SigSubPacket -> Bool
isIssuerSSP (SigSubPacket _ (Issuer _)) = True
isIssuerSSP _ = False

isIssuerFPSSP :: SigSubPacket -> Bool
isIssuerFPSSP (SigSubPacket _ (IssuerFingerprint _ _)) = True
isIssuerFPSSP _ = False

isKET :: SigSubPacket -> Bool
isKET (SigSubPacket _ (KeyExpirationTime _)) = True
isKET _ = False

isKUF :: SigSubPacket -> Bool
isKUF (SigSubPacket _ (KeyFlags _)) = True
isKUF _ = False

isPHA :: SigSubPacket -> Bool
isPHA (SigSubPacket _ (PreferredHashAlgorithms _)) = True
isPHA _ = False

isRevocationKeySSP :: SigSubPacket -> Bool
isRevocationKeySSP (SigSubPacket _ RevocationKey {}) = True
isRevocationKeySSP _ = False

isSigCreationTime :: SigSubPacket -> Bool
isSigCreationTime (SigSubPacket _ (SigCreationTime _)) = True
isSigCreationTime _ = False
